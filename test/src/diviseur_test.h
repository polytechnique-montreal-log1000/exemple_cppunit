// Classe qui teste la classe Diviseur
// Avec le framework CppUnit

// Librairies CppUnit nécessaires.
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Le fichier à tester, qui se trouve dans un répertoire différent.
#include "../../src/diviseur.h"

class DiviseurTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(DiviseurTest);
    CPPUNIT_TEST(test_modulo_diviseur_tres_negatif);
    CPPUNIT_TEST(test_modulo_diviseur_negatif);
    CPPUNIT_TEST(test_modulo_diviseur_nul);
    CPPUNIT_TEST(test_modulo_diviseur_positif);
    CPPUNIT_TEST(test_modulo_diviseur_tres_positif);
    CPPUNIT_TEST_SUITE_END();
    
private:
	Diviseur* objet_a_tester;
    
public:
	// Fonctions d'échafaudage
    void setUp();
    void tearDown();
    
    // Fonctions de tests
    void test_modulo_diviseur_tres_negatif();
    void test_modulo_diviseur_negatif();
    void test_modulo_diviseur_nul();
    void test_modulo_diviseur_positif();
    void test_modulo_diviseur_tres_positif();
};




