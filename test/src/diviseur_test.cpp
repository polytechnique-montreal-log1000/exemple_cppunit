#include "diviseur_test.h"

void DiviseurTest::setUp() {
		this->objet_a_tester = new Diviseur();
}

void DiviseurTest::tearDown() {
		delete this->objet_a_tester;
}

// Correspond à d1 = <{100, -95}, {0}>
void DiviseurTest::test_modulo_diviseur_tres_negatif() {
	int modulo = this->objet_a_tester->modulo(100, -95);
	CPPUNIT_ASSERT_EQUAL(0, modulo);
}

// Correspond à d2 = <{100, -3}, {0}>
void DiviseurTest::test_modulo_diviseur_negatif() {
	int modulo = this->objet_a_tester->modulo(100, -3);
	CPPUNIT_ASSERT_EQUAL(0, modulo);
}

// Correspond à d3 = <{100, 0}, {0}>
void DiviseurTest::test_modulo_diviseur_nul()  {
	int modulo = this->objet_a_tester->modulo(100, 0);
	CPPUNIT_ASSERT_EQUAL(0, modulo);
}

// Correspond à d4 = <{100, 3}, {1}>
void DiviseurTest::test_modulo_diviseur_positif()  {
	int modulo = this->objet_a_tester->modulo(100, 3);
	CPPUNIT_ASSERT_EQUAL(1, modulo);
}

// Correspond à d5 = <{100, 95}, {5}>
void DiviseurTest::test_modulo_diviseur_tres_positif()  {
	int modulo = this->objet_a_tester->modulo(100, 95);
	CPPUNIT_ASSERT_EQUAL(5, modulo);
}



