# Exécutables
EXEC=exemple
EXECTEST=exemple_test
# Répertoires
SOURCE=src
BINAIRE=bin
TESTS=test

all:launch

# =======================
# Compilation du logiciel
# =======================

$(BINAIRE)/$(EXEC): $(BINAIRE)/main.o $(BINAIRE)/diviseur.o
	g++ -o $@ $^

$(BINAIRE)/main.o: $(SOURCE)/main.cpp $(SOURCE)/diviseur.h
	mkdir -p $(BINAIRE)
	g++ -o $@ -c $<

$(BINAIRE)/diviseur.o: $(SOURCE)/diviseur.cpp $(SOURCE)/diviseur.h
	g++ -o $@ -c $<

launch: $(BINAIRE)/$(EXEC)
	./$(BINAIRE)/$(EXEC) 161 13

# ===========
# Utilitaires
# ===========

clean:
	rm -rf $(BINAIRE)/*.o
	rm -rf $(BINAIRE)/$(EXEC)
	rm -rf $(TESTS)/$(BINAIRE)/*.o
	rm -rf $(TESTS)/$(BINAIRE)/$(EXECTEST)

# =================
# Tests du logiciel
# =================

test: $(TESTS)/$(BINAIRE)/$(EXECTEST)
	./$(TESTS)/$(BINAIRE)/$(EXECTEST)

$(TESTS)/$(BINAIRE)/$(EXECTEST): $(TESTS)/$(BINAIRE)/main.o $(TESTS)/$(BINAIRE)/diviseur_test.o $(TESTS)/$(BINAIRE)/diviseur.o
	g++ -o $@ $^ -lcppunit

$(TESTS)/$(BINAIRE)/main.o: $(TESTS)/$(SOURCE)/main.cpp $(TESTS)/$(SOURCE)/diviseur_test.h
	mkdir -p $(TESTS)/$(BINAIRE)
	g++ -o $@ -c $<

$(TESTS)/$(BINAIRE)/diviseur_test.o: $(TESTS)/$(SOURCE)/diviseur_test.cpp $(TESTS)/$(SOURCE)/diviseur_test.h $(SOURCE)/diviseur.h
	g++ -o $@ -c $<

$(TESTS)/$(BINAIRE)/diviseur.o: $(SOURCE)/diviseur.cpp $(SOURCE)/diviseur.h
	g++ -o $@ -c $<

